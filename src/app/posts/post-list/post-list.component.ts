import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Post } from '../post.model';
import { PostService } from '../post.service';
import { from } from 'rxjs';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css']
})
export class PostListComponent implements OnInit, OnDestroy {

  // posts = [
  //   { title: 'First Post', content: 'This is the first\'s content'},
  //   { title: 'Second Post', content: 'This is the second post\'s content'},
  //   { title: 'Third Post', content: 'This is the third post\'s content'}
  // ];
  posts: Post[] = [];
  private postsSubscription: Subscription;

  constructor(public postService: PostService) { }

  ngOnInit() {
    this.posts = this.postService.getAll();
    this.postsSubscription = this.postService.getPostUpdatedListener()
      .subscribe(
        (posts: Post[]) => this.posts = posts
      );
  }
  
  ngOnDestroy(): void {
    this.postsSubscription.unsubscribe();
  }

}
